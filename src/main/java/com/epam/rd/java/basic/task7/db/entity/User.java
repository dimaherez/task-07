package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public boolean equals(Object object){
		return Objects.equals(this.login, ((User) object).login);
	}

	@Override
	public String toString(){
		return this.login;
	}

	public static User createUser(String login) {
		User newUser = new User();
		newUser.setLogin(login);
		return newUser;
	}

}
