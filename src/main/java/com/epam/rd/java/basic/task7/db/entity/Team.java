package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class Team {

	private int id;

	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object object){
		return Objects.equals(this.name, ((Team) object).name);
	}

	@Override
	public String toString(){
		return this.name;
	}

	public static Team createTeam(String name) {
		Team team = new Team();
		team.setName(name);
		return team;
	}

}
