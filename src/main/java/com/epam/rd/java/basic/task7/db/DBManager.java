package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;

	private static  String DATABASE_NAME = "test2db";
	private static  String USER = "task07User";
	private static  String PASSWORD = "1234";
	//private static String URL = "jdbc:sqlserver://localhost;databaseName="+DATABASE_NAME+";user="+USER+";password="+PASSWORD;

	private static final String URL = "jdbc:derby:memory:testdb;create=true";


	public static synchronized DBManager getInstance(){
		try(Connection connection = DriverManager.getConnection(URL);
			Statement stmt = connection.createStatement();){

			stmt.executeUpdate("DELETE FROM users WHERE login = 'ivanov'");
			stmt.executeUpdate("DELETE FROM teams WHERE name = 'teamA'");
		}catch (SQLException e){

			e.printStackTrace();
		} finally {
		}

		if (instance == null) {
			instance = new DBManager();
			return instance;
		}

		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> listOfUsers = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(URL);
			 Statement stmt = connection.createStatement();){

			ResultSet rs = stmt.executeQuery("SELECT id, login FROM users");
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				listOfUsers.add(user);
			}

			return listOfUsers;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listOfUsers;
	}

	public boolean insertUser(User user) throws DBException {
		try (Connection connection = DriverManager.getConnection(URL);
			 Statement stmt = connection.createStatement();){


			ResultSet rs = stmt.executeQuery("SELECT login FROM users WHERE login = '"+user.getLogin()+"'");
			if (rs.next()){
				return false;
			}

			int result = stmt.executeUpdate("INSERT INTO users (login) VALUES ('"+user.getLogin()+"')");
			if (result == 1) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try (Connection connection = DriverManager.getConnection(URL);
			 Statement stmt = connection.createStatement();){

			int result = 0;
			for (User u : users) {
				result += stmt.executeUpdate("DELETE FROM users WHERE login = '"+ u.getLogin()+"'");
			}
			if (result > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		try (Connection connection = DriverManager.getConnection(URL);
			 Statement stmt = connection.createStatement();){

			ResultSet rs = stmt.executeQuery("SELECT id, login FROM users WHERE login = '"+login+"'");

			while (rs.next()) {
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
			}

			return user;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		try (Connection connection = DriverManager.getConnection(URL);
			 Statement stmt = connection.createStatement();){

			ResultSet rs = stmt.executeQuery("SELECT id, name FROM teams WHERE name = '"+name+"'");

			while (rs.next()) {
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
			}

			return team;

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> listOfTeams = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(URL);
			 Statement stmt = connection.createStatement();){

			ResultSet rs = stmt.executeQuery("SELECT id, name FROM teams");
			while (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				listOfTeams.add(team);
			}

			return listOfTeams;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listOfTeams;
	}

	public boolean insertTeam(Team team) throws DBException {
		try (Connection connection = DriverManager.getConnection(URL);
			 Statement stmt = connection.createStatement();){

			ResultSet rs = stmt.executeQuery("SELECT name FROM teams WHERE name = '"+team.getName()+"'");
			if (rs.next()){
				return false;
			}

			int result = stmt.executeUpdate("INSERT INTO teams (name) VALUES ('"+team.getName()+"')");
			if (result == 1) {
				//ResultSet id = stmt.executeQuery("SELECT @@IDENTITY as 'id'");
				//ResultSet id = stmt.executeQuery("SELECT IDENT_CURRENT('teams') as 'id'");
				ResultSet id = stmt.executeQuery("SELECT id FROM teams WHERE name ='"+team.getName()+"'");
				if (id.next()) {
					int teamId = id.getInt("id");
					team.setId(teamId);
				}
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection connection = null;
		Statement stmt = null;

		try {
			connection = DriverManager.getConnection(URL);
			connection.setAutoCommit(false);

			stmt = connection.createStatement();

			ResultSet selectUser = stmt.executeQuery("SELECT id, login FROM users WHERE login = '"+user.getLogin()+"'");
			if (!selectUser.next()){
				return false;
			}

			int userId = selectUser.getInt("id");

			int result = 0;
			for (Team t : teams){
				ResultSet selectTeam = stmt.executeQuery("SELECT id, name FROM teams WHERE name = '"+t.getName()+"'");
				if (!selectTeam.next()){
					return false;
				}

				result += stmt.executeUpdate("INSERT INTO users_teams (user_id, team_id) VALUES ("+userId+","+selectTeam.getInt("id")+")");
			}

			connection.commit();

			if (result > 0) {
				return true;
			} else{
				try{
					connection.rollback();
				} catch (SQLException ex){
					ex.printStackTrace();
				}
				return false;
			}

		} catch (SQLException e) {
			try{
				connection.rollback();
			} catch (SQLException ex){
				ex.printStackTrace();
			}

			throw new DBException("DBException", e);

			//return false;
			//throw new DBException("message", e);
		}finally {
			try {
				connection.commit();
				connection.close();
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> listOfTeams = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(URL);
			 Statement stmt = connection.createStatement();){

			ResultSet selectUser = stmt.executeQuery("SELECT id FROM users WHERE login ='"+user.getLogin()+"'");
			if (selectUser.next()) {
				int userId = selectUser.getInt("id");

				ResultSet rs = stmt.executeQuery(
						"SELECT teams.id, teams.name FROM users_teams\n" +
								"INNER JOIN users\n" +
								"ON users_teams.user_id = users.id\n" +
								"INNER JOIN teams\n" +
								"ON users_teams.team_id = teams.id\n" +
								"WHERE user_id = "+userId);

				while (rs.next()) {
					Team team = new Team();
					team.setId(rs.getInt("id"));
					team.setName(rs.getString("name"));
					listOfTeams.add(team);
				}
			}



			return listOfTeams;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return listOfTeams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (Connection connection = DriverManager.getConnection(URL);
			 Statement stmt = connection.createStatement();){

			int result = stmt.executeUpdate("DELETE FROM teams WHERE name = '"+ team.getName()+"'");

			if (result > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		try (Connection connection = DriverManager.getConnection(URL);
			 Statement stmt = connection.createStatement();){

			int result = stmt.executeUpdate("UPDATE teams SET name = '" + team.getName() + "' WHERE id = " + team.getId());

			if (result > 0) {
				return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

}
